# M5STACK

Installation du firmware UIFlow-v1.2.3-en via [M5Burner sous windows](https://m5stack.readthedocs.io/en/master/get-started/how_to_burn_firmware_en.html)

https://www.m5stack.com/download

https://m5stack.oss-cn-shenzhen.aliyuncs.com/resource/software/M5Burner.zip

Utiliser virtualbox avec partage COM1 de /dev/ttyUSB0

La connexion via [UIFlow](http://flow.m5stack.com/) fonctionne.