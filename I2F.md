# I2F

Relier via I2F le M5Stack au PZEM-004T

I2F via Micropython : https://github.com/m5stack/M5Stack_MicroPython#i2c

https://docs.micropython.org/en/latest/library/machine.I2C.html


Le programme suivant affiche un periph i2c quand on appuie sur le bouton B

```python

from m5stack import *
from m5ui import *
import time
from machine import I2C

i2c = I2C(freq=400000, sda=21, scl=22)
# create I2C peripheral at frequency of 400kHz
# depending on the port, extra parameters may be required
# to select the peripheral and/or pins to use


clear_bg(0xac3b3b)



btnA = M5Button(name="ButtonA", text="ButtonA", visibility=True)
btnB = M5Button(name="ButtonB", text="ButtonB", visibility=True)
btnC = M5Button(name="ButtonC", text="ButtonC", visibility=True)
rectangle1 = M5Rect(97, 54, 30, 30, 0xFFFFFF, 0xFFFFFF)
circle1 = M5Circle(241, 97, 15, 0xFFFFFF, 0xFFFFFF)
title1 = M5Title(title="Title", fgcolor=0xFFFFFF, bgcolor=0x0000FF)
label1 = M5TextBox(23, 98, "Text", lcd.FONT_Default, 0xfff906)

def buttonA_pressed():
  scan = i2c.scan()                      # scan for slaves, returning a list of 7-bit addresses
  msg = str(scan)
  rectangle1 = M5Rect(45, 120, 120, 60, 0x222222, 0xFFFFFF)
  label2 = M5TextBox(50, 140, msg, lcd.FONT_Default, 0xfff906)

def buttonB_pressed():
  # global params
  msg = str(int(time.time())) + "\nrrr"
  rectangle1 = M5Rect(45, 120, 120, 60, 0x222222, 0xFFFFFF)
  label2 = M5TextBox(50, 140, msg, lcd.FONT_Default, 0xfff906)
    

def buttonC_pressed():
  # global params
  pass

buttonB.wasPressed(callback=buttonB_pressed)
buttonC.wasPressed(callback=buttonC_pressed)
buttonA.wasPressed(callback=buttonA_pressed)

```
