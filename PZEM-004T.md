
Sensor ref :
* PZEM-004T(v2.0)
* 2B6770001A
* peacefair

Liens utiles :
* [Fournisseur bangdood](https://fr.banggood.com/)PZEM-004T-Maximum-100A-AC-Multifunction-Power-Monitoring-Communications-Module-Monitor-Module-p-1021915.html
* [Explications](https://github.com/olehs/PZEM004T/wiki/Basic-Setup)
* [PZEM + raspberry + esp32](http://pdacontrolen.com/meter-pzem-004t-with-arduino-esp32-esp8266-python-raspberry-pi/)
* [openenergymonitor](https://openenergymonitor.org/)

Il faut un convertisseur TTL 5V vers USB comme [celui-ci](https://www.banggood.com/FT232RL-FTDI-USB-To-TTL-Serial-Converter-Adapter-Module-For-Arduino-p-917226.html?p=6T171512609262015028&cur_warehouse=USA).
plutôt en USB classique comme [celui-ci](https://www.banggood.com/6-In-1-CP2102-USB-To-TTL-485-232-Converter-3_3V-5V-Compatible-Six-Multifunction-Serial-Module-p-1200130.html?rmmds=search&cur_warehouse=CN)


Module commandé à 7,05€ : https://www.banggood.com/PZEM-004T-Maximum-100A-AC-Multifunction-Power-Monitoring-Communications-Module-Monitor-Module-p-1021915.html

![schema brocahge](2016051404170811-1021915.jpg)

![schema logique](01b46f70-d244-4911-873b-453278a49233.jpg)

9600 bauds, 8 bits, pair, 1 bit stop, pair

